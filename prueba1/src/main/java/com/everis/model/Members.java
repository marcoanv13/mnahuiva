package com.everis.model;


public class Members {
private Integer familyMember;
private String firstName;
private String lastName;
private String familyName;
private String parentOrStudentMember;




/**
* Constructor de la clase Memmbers
* @author Marco
*/
public Members(Integer familyMember, String firstName, String lastName, String familyName,String parentOrStudentMember) {
super();
this.familyMember = familyMember;
this.firstName = firstName;
this.lastName = lastName;
this.familyName = familyName;
this.parentOrStudentMember=parentOrStudentMember;
}

public String getParentOrStudentMember() {
return parentOrStudentMember;
}

public void setParentOrStudentMember(String parentOrStudentMember) {
this.parentOrStudentMember = parentOrStudentMember;
}

public Integer getFamilyMember() {
return familyMember;
}

public void setFamilyMember(Integer familyMember) {
this.familyMember = familyMember;
}

public String getFirstName() {
return firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getFamilyName() {
return familyName;
}

public void setFamilyName(String familyName) {
this.familyName = familyName;
}

}
