package com.everis.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

//Clase donde carga los campos de la entidad Students
@Entity
@Table(name = "Students")
public class Students {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer studentId;
private String firstName;
private String gender;
private String middleName;
private String lastName;
private String dateOfBirth;
private String otherStudentDetails;

@ManyToMany
@JoinTable(name = "Students_Parents",joinColumns =
@JoinColumn(name = "student_id"),inverseJoinColumns = @JoinColumn(name = "parent_id"))
Set<Parents> likedCourses;

public Integer getStudentId() {
return studentId;
}


public void setStudentId(Integer studentId) {
this.studentId = studentId;
}


public String getGender() {
return gender;
}


public void setGender(String gender) {
this.gender = gender;
}


public String getFirstName() {
return firstName;
}


public void setFirstName(String firstName) {
this.firstName = firstName;
}


public String getMiddleName() {
return middleName;
}


public void setMiddleName(String middleName) {
this.middleName = middleName;
}


public String getLastName() {
return lastName;
}


public void setLastName(String lastName) {
this.lastName = lastName;
}


public String getDateOfBirth() {
return dateOfBirth;
}


public void setDateOfBirth(String dateOfBirth) {
this.dateOfBirth = dateOfBirth;
}


public String getOtherStudentDetails() {
return otherStudentDetails;
}


public void setOtherStudentDetails(String otherStudentDetails) {
this.otherStudentDetails = otherStudentDetails;
}

}
