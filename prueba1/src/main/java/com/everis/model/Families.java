package com.everis.model;



import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//Clase donde carga los campos de la entidad Families
@JsonIgnoreProperties(ignoreUnknown = true)
@Entity
@Table(name = "Families")
public class Families {
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer familyId;

private String familyName;

@ManyToOne
@JoinColumn(name="parent_id", nullable=false)
private Parents foreigParents;

@OneToMany(mappedBy = "family")
private Set<FamilyMembers> foreignFamily;

public Parents getForeigParents(){
return foreigParents;
}


public void setForeigParents(Parents foreigParents){
this.foreigParents = foreigParents;
}

public Integer getFamilyId(){
return familyId;
}


public void setFamilyId(Integer familyId){
this.familyId = familyId;
}


public String getFamilyName(){
return familyName;
}


public void setFamilyName(String familyName){
this.familyName = familyName;
}

}
