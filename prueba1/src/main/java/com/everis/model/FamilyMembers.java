package com.everis.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


//Clase donde carga los campos de la entidad FamilyMembers
@Entity
@Table(name = "Family_Members")
public class FamilyMembers {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer familyMemberId;
@ManyToOne
@JoinColumn(name= "student_id")
private Students student;
 
@ManyToOne
@JoinColumn(name = "parent_id")
private Parents parent;
    
@ManyToOne
@JoinColumn(name = "family_id")
private Families family;
    
    
private String parentOrStudentMember;
    

    
public Integer getFamilyMemberId() {
return familyMemberId;
}


public void setFamilyMemberId(Integer familyMemberId) {
this.familyMemberId = familyMemberId;
}


public Students getStudent() {
return student;
}


public void setStudent(Students student) {
this.student = student;
}


public Parents getParent() {
return parent;
}


public void setParent(Parents parent) {
this.parent = parent;
}


public Families getFamily() {
return family;
}


public void setFamily(Families family) {
this.family = family;
}


public String getParentOrStudentMember() {
return parentOrStudentMember;
}


public void setParentOrStudentMember(String parentOrStudentMember) {
this.parentOrStudentMember = parentOrStudentMember;
}



}
