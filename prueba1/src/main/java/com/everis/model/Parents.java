package com.everis.model;

import java.util.Set;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//Clase donde carga los campos de la entidad Parents
@Entity
@Table(name = "Parents")
public class Parents {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Integer parentId;
private String gender;
private String firstName;
private String middleName;
private String lastName;
private String otherParentDetails;
@OneToMany(mappedBy = "parent")
private Set<FamilyMembers> foreignParent;

public Integer getParentId() {
return parentId;
}

public void setParentId(Integer parentId) {
this.parentId = parentId;
}

public String getGender() {
return gender;
}

public void setGender(String gender) {
this.gender = gender;
}

public String getFirstName() {
return firstName;
}

public void setFirstName(String firstName) {
this.firstName = firstName;
}

public String getMiddleName() {
return middleName;
}

public void setMiddleName(String middleName) {
this.middleName = middleName;
}

public String getLastName() {
return lastName;
}

public void setLastName(String lastName) {
this.lastName = lastName;
}

public String getOtherParentDetails() {
return otherParentDetails;
}

public void setOtherParentDetails(String otherParentDetails) {
this.otherParentDetails = otherParentDetails;
}

}
