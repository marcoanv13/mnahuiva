package com.everis.prueba1;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.everis.repository")
@EntityScan(basePackages = {"com.everis.model"})
@ComponentScan(basePackages = "com.everis")
public class Prueba1Application {
public static void main(String[] args) {
SpringApplication.run(Prueba1Application.class, args);
}


}
