package com.everis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.everis.model.Students;

//interface de la clase Students 
@Repository
public interface StudentsRepository extends JpaRepository<Students, Integer>  {

}
