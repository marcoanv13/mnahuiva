package com.everis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.everis.model.Parents;

//interface de la clase Parents 
@Repository
public interface ParentsRepository extends  JpaRepository<Parents, Integer> {


}
