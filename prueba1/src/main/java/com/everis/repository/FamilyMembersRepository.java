package com.everis.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.stereotype.Repository;

import com.everis.model.FamilyMembers;

//Repository de la clase FamilyMembers 
@Repository
public interface FamilyMembersRepository extends  JpaRepository<FamilyMembers, Integer> {

}
