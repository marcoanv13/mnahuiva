package com.everis.repository;

import java.util.List;

import com.everis.model.Members;

//interface de la clase Members 
public interface MembersRepository {
public List<Members> getFamilyById(Integer id);
}
