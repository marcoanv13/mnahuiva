package com.everis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.everis.model.Families;

//Repository de la clase Families 
@Repository
public interface FamiliesRepository extends  JpaRepository<Families, Integer> {

}
