package com.everis.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.everis.model.Members;

//Repository de la clase Members 
@Repository
public class MembersRepositoryImplement implements MembersRepository  {

@Autowired
private JdbcTemplate jdbcTemplate;

@Override
public List<Members> getFamilyById(Integer id) {

StringBuilder sql=new StringBuilder(); 
sql.append("select  fm.family_member_id, p.first_name, p.last_name, f.family_name,fm.parent_or_student_member");
sql.append(" from Family_Members fm ");
sql.append(" INNER JOIN parents p ON p.parent_id=fm.parent_id ");
sql.append(" INNER JOIN families f on f.family_id=fm.family_id ");
sql.append(" INNER JOIN students s on s.student_id=fm.student_id where fm.family_id = ?");


return jdbcTemplate.query(sql.toString(),new Object[]{id},(rs, rowNum) ->new Members(rs.getInt("family_member_id"), rs.getString("first_name"), rs.getString("last_name"),rs.getString("family_name"),rs.getString("parent_or_student_member")));

}
}
