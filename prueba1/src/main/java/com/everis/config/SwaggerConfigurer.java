package com.everis.config;


import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Esta clase se encarga de inicializar configuracion del swaggers
 * @return void
*/

@Configuration
@EnableSwagger2
public class SwaggerConfigurer {



/**
* Este método se encarga de documentar los controllers con swagger 
* @author Marco
	*/
	
@Bean
public Docket api() { 
return new Docket(DocumentationType.SWAGGER_2)  
.select()                                  
.apis(RequestHandlerSelectors.basePackage("com.everis.controller"))              
.paths(PathSelectors.any())                          
.build();                                           
}



}
