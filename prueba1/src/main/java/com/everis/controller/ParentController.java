package com.everis.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.everis.model.Parents;
import com.everis.service.ParentService;


/**
 * Esta clase se encarga de contener los controladores de la clase Parents
 * @return void
*/
@RestController
public class ParentController {

@Autowired
private ParentService parentService;
/**
* Este método obtiene todos los parientes registrados
* @author Marco
* @return lista de parientes  registrados
*/

@GetMapping("/parent")
public List<Parents> getAllParent() {
return parentService.getAllParent();
}

/**
* Este método se encarga de registrar datos de los parientes   
* @author Marco
* @param parent 
* parent para realizar la insercion de parientes 
*/

@PostMapping("/parent")
public void addParent(@RequestBody Parents parent){
parentService.addParent(parent);
}

/**
* Este método se encarga de obtener datos de los pariente segun id 
* @author Marco
* @param id
* id es un codigo de pariente  a buscar 
* @return datos de pariente a buscar
*/

@GetMapping("/parent/{id}")
public Optional<Parents> getParentById(@PathVariable Integer id){
return parentService.getParentById(id);
}

/**
* Este método se encarga de actualizar pariente
* @author Marco
* @param parent
* objeto Parents para realizar la modificacion de pariente
*/
@PutMapping("/parent")
public void updateParent(@RequestBody Parents parent){
parentService.updateParent(parent);
}

/**
* Este método se encarga de eliminar parientes
* @author Marco
* @param parent
* objeto Parents para realizar la eliminacion de parientes
 */
@DeleteMapping("/parent")
public void deleteParent(@RequestBody Parents parent) {
parentService.deleteParent(parent);
}

}
