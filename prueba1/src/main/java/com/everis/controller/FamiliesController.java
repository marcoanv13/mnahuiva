package com.everis.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.everis.model.Families;
import com.everis.service.FamiliesService;

/**
 * Esta clase se encarga de contener los controladores de la clase Families
 * @return void
*/
@RestController
public class FamiliesController {
@Autowired
private FamiliesService familiesService;
/**
* Este método obtiene todas las familias registradas
* @author Marco
* @return lista de familia registradas
*/

@GetMapping("/families")
public List<Families> getAllFamilies() {
return familiesService.getAllFamilies();
}

/**
* Este método  registra familias
* @author Marco
*/
@PostMapping("/families")
public void addFamilies(@RequestBody Families families) {
familiesService.addFamilies(families);
}

/**
 * Este método se encarga de obtener datos de la familia segun id 
 *  @author Marco
 * @param id
 * id es un codigo con la familia a buscar 
 * @return datos de familia a buscar
*/

@GetMapping("/families/{id}")
public Optional<Families> getFamiliesById(@PathVariable Integer id) {
return familiesService.getFamiliesById(id);
}

/**
* Este método se encarga de actualizar familia
* @author Marco
* @param families
* objeto Families para realizar la modificacion de la familia 
*/ 
@PutMapping("/families")
public void updateFamilies(@RequestBody Families families) {
familiesService.updateFamilies(families);
}
/**
* Este método se encarga de eliminar familia
* @author Marco
* @param families
* objeto familia para realizar la eliminacion de la familia 
*/

@DeleteMapping("families")
public void deleteFamilies(@RequestBody Families families) {
familiesService.deleteFamilies(families);
}

}
