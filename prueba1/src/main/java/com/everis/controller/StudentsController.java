package com.everis.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.everis.model.Students;
import com.everis.service.StudentsService;

/**
 * Esta clase se encarga de contener los controladores de la clase Students
 * @return void
*/
@RestController
public class StudentsController {

@Autowired
private StudentsService studentService;

/**
* Este método obtiene todos los estudiantes registrados
* @author Marco
* @return lista de estudiantes  registrados
*/
@GetMapping("/students")
public List<Students> getAllStudents(){
return studentService.getAllStudents();
}

/**
* Este método se encarga de registrar datos de  estudiante   
* @author Marco
* @param students
* objeto Students para realizar la insercion de estudiante
*/
@PostMapping("/students")
public void addStudents(@RequestBody Students students){
studentService.addStudents(students);
}

/**
 * Este método se encarga de obtener datos de estudiante segun id 
 * @author Marco
 * @param id
* id es un codigo de estudiante  a buscar 
 * @return datos de estudiante a buscar
*/
@GetMapping("/students/{id}")
public Optional<Students> getStudentsById(@PathVariable Integer id){
return studentService.getStudentsById(id);
}

/**
* Este método se encarga de actualizar estudiante
* @author Marco
* @param students
* objeto Students para realizar la modificacion de estudiante
*/
@PutMapping("/students")
public void updateStudents(@RequestBody Students students){
studentService.updateStudents(students);
}

/**
* Este método se encarga de eliminar estudiante
* @author Marco
* @param students
* objeto Students para realizar la eliminacion de estudiante
*/
@DeleteMapping("/students")
public void deleteStudents(@RequestBody Students students){
studentService.deleteStudents(students);
}

}
