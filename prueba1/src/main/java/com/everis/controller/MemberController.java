package com.everis.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import com.everis.model.Members;
import com.everis.service.MembersService;


/**
 * Esta clase se encarga de contener los controladores de la clase Members
 * @return void
*/
@RestController
public class MemberController {

@Autowired
private MembersService memberService;

/**
* Este método se encarga de obtener datos de los miembros de familia 
* sabiendo que datos como codigo,nombre y a que familia corresponde 
* @author Marco
* @param id 
* es un codigo con la familia a buscar 
* @return datos de miembros de familia
*/
@GetMapping("/members/{id}")
public List<Members> getFamilyById(@PathVariable Integer id) {
return memberService.getFamilyById(id);
}

}
