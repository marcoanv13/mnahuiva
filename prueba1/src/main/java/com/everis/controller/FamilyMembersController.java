package com.everis.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.everis.model.FamilyMembers;
import com.everis.service.FamilyMemberService;

/**
 * Esta clase se encarga de contener los controladores de la clase FamilyMembers
 * @return void
*/
@RestController
public class FamilyMembersController {

@Autowired
private FamilyMemberService familieMembersService;

/**
* Este método obtiene todas los miembros de familia registradas
* @author Marco
* @return lista de miembros de familia  registrados
*/
@GetMapping("/familyMembers")
public List<FamilyMembers> getAllFamilyMember() {
return familieMembersService.getAllFamilyMember();
}

/**
* Este método se encarga de registrar datos de los miembros de familia   
* @author Marco
* @param familiesMembers
* objeto FamilyMembers para realizar la insercion de miembros de familia
*/

@PostMapping("/familyMembers")
public void addFamilyMember(@RequestBody FamilyMembers familiesMembers) {
familieMembersService.addFamilyMember(familiesMembers);
}

/**
* Este método se encarga de obtener datos de los miembros de familia segun id 
* @author Marco
* @param id
* id es un codigo con la familia a buscar 
* @return datos de miembros de familia a buscar
*/
@GetMapping("/familyMembers/{id}")
public Optional<FamilyMembers> getFamilyMemberById(@PathVariable Integer id) {
return familieMembersService.getFamilyMemberById(id);
}

/**
* Este método se encarga de actualizar miembros de familia
* @author Marco
* @param familiesMembers
* objeto FamilyMembers para realizar la modificacion de miembros de familia
*/
@PutMapping("/familyMembers")
public void updateFamilyMember(@RequestBody FamilyMembers familiesMembers) {
familieMembersService.updateFamilyMember(familiesMembers);
}

/**
* Este método se encarga de eliminar miembros de familia
* @author Marco
* @param familiesMembers
* objeto FamilyMembers para realizar la eliminacion de miembros de familia 
*/
@DeleteMapping("/familyMembers")
public void deleteFamilyMember(@RequestBody FamilyMembers familiesMembers){
familieMembersService.deleteFamilyMember(familiesMembers);
}

}
