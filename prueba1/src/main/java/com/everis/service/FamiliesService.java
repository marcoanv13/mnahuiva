package com.everis.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.everis.model.Families;
import com.everis.repository.FamiliesRepository;



/**
 * Esta clase se encarga de contener los servicios de la clase Families
 * @return void
*/
@Service
public class FamiliesService {

@Autowired
private FamiliesRepository familiesRepo;

/**
* Este método obtiene todas las familias registradas
* @author Marco
* @return lista de familia registradas
*/
public List<Families> getAllFamilies() {
List<Families> families = new ArrayList<>();
familiesRepo.findAll().stream().forEach(families::add);
return families;
}
 
/**
* Este método  registra familias
* @author Marco
*/
public void addFamilies(Families families) {
familiesRepo.save(families);
}

/**
* Este método se encarga de obtener datos de la familia segun id 
* @author Marco
* @param id es un codigo con la familia a buscar 
* @return datos de familia a buscar
*/
public Optional<Families> getFamiliesById(Integer id) {
return familiesRepo.findById(id);
}
/**
* Este método se encarga de actualizar familia
* @author Marco
* @param families
* objeto familia para realizar la modificacion de la familia 
 */

public void updateFamilies(Families families) {
familiesRepo.save(families);
}

/**
* Este método se encarga de eliminar familia
* @author Marco
* @param families
* objeto familia para realizar la eliminacion de la familia 
*/

public void deleteFamilies(Families families) {
familiesRepo.delete(families);
}


}
