package com.everis.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.model.Parents;
import com.everis.model.Students;
import com.everis.repository.StudentsRepository;

/**
 * Esta clase se encarga de contener los servicios de la clase Students
 * @return void
*/
@Service
public class StudentsService {
@Autowired
private StudentsRepository studentsRepo;
/**
* Este método obtiene todos los estudiantes registrados
* @author Marco
* @return lista de estudiantes  registrados
*/

public List<Students> getAllStudents() {
List<Students> students= new ArrayList<>();
studentsRepo.findAll().stream().forEach(students::add);
return students;
}
/*
* Este método se encarga de registrar datos de  estudiante   
* @author Marco
* @param objeto Students para realizar la insercion de estudiante 
* @return void
*/

public void addStudents(Students students) {
studentsRepo.save(students);
}
/**
* Este método se encarga de obtener datos de estudiante segun id 
* @author Marco
* @param id es un codigo de estudiante  a buscar 
* @return datos de estudiante a buscar
*/

public Optional<Students> getStudentsById(Integer id) {
return studentsRepo.findById(id);
}

/**
* Este método se encarga de actualizar estudiante
* @author Marco
* @param students
* objeto Students para realizar la modificacion de estudiante
*/
public void updateStudents(Students students) {
studentsRepo.save(students);
}
/**
* Este método se encarga de eliminar estudiante
* @author Marco
* @param students
* objeto Students para realizar la eliminacion de estudiante
*/

public void deleteStudents(Students students) {
studentsRepo.delete(students);
}

}
