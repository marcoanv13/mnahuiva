package com.everis.service;

import com.everis.model.FamilyMembers;
import com.everis.repository.FamilyMembersRepository;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;






/**
 * Esta clase se encarga de contener los servicios de la clase FamilyMember
 * @return void
*/
@Service
public class FamilyMemberService  {

@Autowired
private FamilyMembersRepository familymemberRepo;

/**
* Este método obtiene todas los miembros de familia registradas
* @author Marco
* @return lista de miembros de familia  registrados
*/
public List<FamilyMembers> getAllFamilyMember(){
List<FamilyMembers> familymember= new ArrayList<>();
familymemberRepo.findAll().stream().forEach(familymember::add);
return familymember;
}

/**
* Este método se encarga de registrar datos de los miembros de familia   
* @author Marco
* @param familymember
* objeto FamilyMembers para realizar la insercion de miembros de familia
*/
public void addFamilyMember(FamilyMembers familymember){
familymemberRepo.save(familymember);
}

/**
* Este método se encarga de obtener datos de los miembros de familia segun id 
* @author Marco
* @param id 
* id es un codigo con la familia a buscar 
* @return datos de miembros de familia a buscar
*/
public Optional<FamilyMembers> getFamilyMemberById(Integer id) {
return familymemberRepo.findById(id);
}

/**
* Este método se encarga de actualizar miembros de familia
* @author Marco
* @param familymember
* objeto FamilyMembers para realizar la modificacion de miembros de familia
*/
public void updateFamilyMember(FamilyMembers familymember) {
familymemberRepo.save(familymember);
}

/**
* Este método se encarga de eliminar miembros de familia
* @author Marco
* @param familymember
* objeto FamilyMembers para realizar la eliminacion de miembros de familia 
*/
public void deleteFamilyMember(FamilyMembers familymember) {
familymemberRepo.delete(familymember);
}

}
