package com.everis.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.model.FamilyMembers;
import com.everis.model.Members;
import com.everis.repository.MembersRepository;
import com.everis.repository.MembersRepositoryImplement;


/**
 * Esta clase se encarga de contener los servicios de la clase MembersService
 * @return void
*/
@Service
public class MembersService implements  MembersRepository {

@Autowired
private MembersRepositoryImplement membersrepo;

/**
* Este método se encarga de obtener datos de los miembros de familia 
* sabiendo que datos como codigo,nombre y a que familia corresponde 
* @author Marco
* @param id es un codigo con la familia a buscar 
* @return datos de miembros de familia
*/

@Override
public List<Members> getFamilyById(Integer id) {
List<Members> members= new ArrayList<>();
membersrepo.getFamilyById(id).stream().forEach(members::add);
return members;
}
}
