package com.everis.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.everis.model.FamilyMembers;
import com.everis.model.Parents;
import com.everis.repository.ParentsRepository;

/**
 * Esta clase se encarga de contener los servicios de la clase Parents
 * @return void
*/
@Service
public class ParentService {
@Autowired
private ParentsRepository parentRepo;

/**
* Este método obtiene todos los parientes registrados
* @author Marco
* @return lista de parientes  registrados
*/
public List<Parents> getAllParent() {
List<Parents> parent= new ArrayList<>();
parentRepo.findAll().stream().forEach(parent::add);
return parent;
}

/**
* Este método se encarga de registrar datos de los parientes   
* @author Marco
* @param parent
* objeto Parents para realizar la insercion de parientes 
*/
public void addParent(Parents parent) {
parentRepo.save(parent);
}

/**
* Este método se encarga de obtener datos de los pariente segun id 
* @author Marco
* @param id
* id es un codigo de pariente  a buscar 
* @return datos de pariente a buscar
*/
public Optional<Parents> getParentById(Integer id) {
return parentRepo.findById(id);
}

/**
* Este método se encarga de actualizar pariente
* @author Marco
 * @param parent
* objeto Parents para realizar la modificacion de pariente

 */
public void updateParent(Parents parent) {
parentRepo.save(parent);
}

/**
* Este método se encarga de eliminar parientes
* @author Marco
* @param parent
* objeto Parents para realizar la eliminacion de parientes
*/
public void deleteParent(Parents parent) {
parentRepo.delete(parent);
}

}
