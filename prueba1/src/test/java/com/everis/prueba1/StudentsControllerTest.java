package com.everis.prueba1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;


import com.everis.model.Students;

public class StudentsControllerTest extends Prueba1ApplicationTests {

@Override
@Before
public void setUp() {
super.setUp();
}

@Test
public void addStudents() throws Exception {
String uri = "/students";
Students students= new Students();
students.setDateOfBirth("10-10-2020");
students.setFirstName("marco");
students.setGender("m");
students.setLastName("nahui vargas");
students.setMiddleName("antonio");
students.setOtherStudentDetails("programador java");
String inputJson = super.mapToJson(students);
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
}

@Test
public void updateStudents() throws Exception {
String uri = "/students";
Students students= new Students();
students.setStudentId(1);
students.setDateOfBirth("10-10-2020");
students.setFirstName("gustavo");
students.setGender("m");
students.setLastName("nahui vargas");
students.setMiddleName("jose");
students.setOtherStudentDetails("programador vb.net");
String inputJson = super.mapToJson(students);
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
}
//@Test
//public void deleteStudents() throws Exception {
//String uri = "/students";
//Students students = new Students();
//students.setStudentId(2);
//String inputJson = super.mapToJson(students);
//MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri).
//contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();  
//int status = mvcResult.getResponse().getStatus();
//assertEquals(200, status);
//}


@Test
public void getStudents() throws Exception {
String uri = "/students";
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
String content = mvcResult.getResponse().getContentAsString();
Students[] studentsList = super.mapFromJson(content, Students[].class);
assertTrue(studentsList.length > 0);
}

@Test
public void getStudentsById() throws Exception {
Integer id=1;
String uri = "/students/"+id+"";
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
String content = mvcResult.getResponse().getContentAsString();
Students students = super.mapFromJson(content, Students.class);
assertTrue(students.getStudentId()==id);
}

}
