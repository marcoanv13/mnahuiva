package com.everis.prueba1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.hibernate.type.descriptor.sql.JdbcTypeFamilyInformation.Family;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.everis.model.Families;
import com.everis.model.FamilyMembers;
import com.everis.model.Parents;
import com.everis.model.Students;

public class FamilyMembersControllerTest extends Prueba1ApplicationTests {
@Override
@Before
public void setUp() {
super.setUp();
}

@Test
public void addFamilyMembers() throws Exception {
String uri = "/familyMembers";
FamilyMembers familiesMember = new FamilyMembers();
Families families = new Families();
families.setFamilyId(2);
Parents parent= new Parents();
parent.setParentId(1);
Students student= new Students();
student.setStudentId(1);
familiesMember.setFamily(families);
familiesMember.setParent(parent);
familiesMember.setStudent(student);
familiesMember.setParentOrStudentMember("padre del estudiante");
String inputJson = super.mapToJson(familiesMember);
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
}

@Test
public void updateFamilyMembers() throws Exception {
String uri = "/familyMembers";
FamilyMembers familiesMember = new FamilyMembers();
Families families = new Families();
families.setFamilyId(2);
Parents parent= new Parents();
parent.setParentId(1);
Students student= new Students();
student.setStudentId(1);
familiesMember.setFamilyMemberId(1);
familiesMember.setFamily(families);
familiesMember.setParent(parent);
familiesMember.setStudent(student);
familiesMember.setParentOrStudentMember("padre del estudiante");
String inputJson = super.mapToJson(familiesMember);
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
}

//
//@Test
//public void deleteFamilyMembers() throws Exception {
//String uri = "/familyMembers";
//FamilyMembers familiesMember = new FamilyMembers();
//familiesMember.setFamilyMemberId(1);
//String inputJson = super.mapToJson(familiesMember);
//MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri).
//contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
//int status = mvcResult.getResponse().getStatus();
//assertEquals(200, status);
//}
//

@Test
public void getFamilyMembers() throws Exception {
String uri = "/familyMembers";
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
String content = mvcResult.getResponse().getContentAsString();
FamilyMembers[] familyMemberlist = super.mapFromJson(content, FamilyMembers[].class);
}

@Test
public void getFamiliesById() throws Exception {
Integer id=12;
String uri = "/familyMembers/"+id+"";
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
String content = mvcResult.getResponse().getContentAsString();
FamilyMembers familyMembers = super.mapFromJson(content, FamilyMembers.class);
assertTrue(familyMembers.getFamilyMemberId()==id);
}

}
