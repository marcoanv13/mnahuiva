package com.everis.prueba1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.everis.model.Families;
import com.everis.model.Parents;

public class FamiliesControllerTest extends Prueba1ApplicationTests {
@Override
@Before
public void setUp() {
super.setUp();
}

@Test
public void addFamilies() throws Exception {
String uri = "/families";
Families families = new Families();
Parents parent=new Parents();
families.setFamilyName("familia 1");
parent.setParentId(1);
parent.setFirstName("marco");
parent.setGender("m");
parent.setLastName("nahui vargas");
parent.setMiddleName("antonio");
parent.setOtherParentDetails("divorciado");
families.setForeigParents(parent);
String inputJson = super.mapToJson(families);
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
}

@Test
public void updateFamilies() throws Exception {
String uri = "/families";
Families families = new Families();
Parents parent=new Parents();
families.setFamilyName("familia n1");
families.setFamilyId(1);
parent.setParentId(1);
parent.setFirstName("marco");
parent.setGender("m");
parent.setLastName("nahui vargas");
parent.setMiddleName("jose");
parent.setOtherParentDetails("casado");
families.setForeigParents(parent);
String inputJson = super.mapToJson(families);
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
}


@Test
public void deleteFamilies() throws Exception {
String uri = "/families";
Families families = new Families();
families.setFamilyId(1);
String inputJson = super.mapToJson(families);
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
}

@Test
public void getFamilies() throws Exception {
String uri = "/families";
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
String content = mvcResult.getResponse().getContentAsString();
Families[] familielist = super.mapFromJson(content, Families[].class);
assertTrue(familielist.length > 0);
}

@Test
public void getFamiliesById() throws Exception {
Integer id=2;
String uri = "/families/"+id+"";
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
String content = mvcResult.getResponse().getContentAsString();
Families families = super.mapFromJson(content, Families.class);
assertTrue(families.getFamilyId()==id);
}



}
