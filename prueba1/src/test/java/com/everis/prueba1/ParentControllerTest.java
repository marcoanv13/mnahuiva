package com.everis.prueba1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.everis.model.Parents;

public class ParentControllerTest extends Prueba1ApplicationTests {

@Override
@Before
public void setUp() {
super.setUp();
}

@Test
public void addParent() throws Exception {
String uri = "/parent";
Parents parent= new Parents();
parent.setFirstName("marco");
parent.setGender("m");
parent.setLastName("nahui vargas");
parent.setMiddleName("antonio");
parent.setOtherParentDetails("divorciado");
String inputJson = super.mapToJson(parent);
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
}

@Test
public void updateParent() throws Exception {
String uri = "/parent";
Parents parent= new Parents();
parent.setParentId(1);
parent.setFirstName("marco");
parent.setGender("m");
parent.setLastName("nahui lopez");
parent.setMiddleName("jose");
parent.setOtherParentDetails("cassado");
String inputJson = super.mapToJson(parent);
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
}
//@Test
//public void deleteParent() throws Exception {
//String uri = "/parent";
//Parents parent = new Parents();
//parent.setParentId(1);
//String inputJson = super.mapToJson(parent);
//MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(uri).
//contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson)).andReturn();
//int status = mvcResult.getResponse().getStatus();
//assertEquals(200, status);
//}


@Test
public void getParent() throws Exception {
String uri = "/parent";
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
String content = mvcResult.getResponse().getContentAsString();
Parents[] parentlist = super.mapFromJson(content, Parents[].class);
assertTrue(parentlist.length > 0);
}

@Test
public void getParentById() throws Exception {
Integer id=1;
String uri = "/parent/"+id+"";
MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri)).andReturn();
int status = mvcResult.getResponse().getStatus();
assertEquals(200, status);
String content = mvcResult.getResponse().getContentAsString();
Parents parent = super.mapFromJson(content, Parents.class);
assertTrue(parent.getParentId()==id);
}
}
